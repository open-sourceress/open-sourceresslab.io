# open-sourceress.gitlab.io

Personal website

## Building
This website is built with [Zola](https://www.getzola.org/).

1. Install Git
1. Install [Zola](https://www.getzola.org/documentation/getting-started/installation/)
1. Optional: fork this repository
1. Clone this repository or your fork
1. In a shell, `cd` into the cloned project and run `zola serve`
1. Open http://localhost:1111 in a browser

## Deploying
### GitLab Pages
1. Fork this repository if you haven't already, and re-run the Building instructions from your fork
1. Rename your fork to match your GitLab Pages domain (e.g. user `example` would rename it to `example.gitlab.io`)
1. Recommended: update `ZOLA_VERSION` in `.gitlab-ci.yml` to match your Zola version
1. Push any changes
1. Open your domain (e.g. https://example.gitlab.io) in a browser
