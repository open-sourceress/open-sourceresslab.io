+++
title = "About Me"
+++
Hi, I'm Cynthia. I'm a college student and software developer-in-training in the Pacific Northwest USA. I write Discord
bots like [Violet](https://gitlab.com/open-sourceress/violet), as well as libraries and tools to solve problems as I
discover them. I code for fun to make coding for fun more fun.

I also mentor a local [FIRST Tech Challenge](https://www.firstinspires.org/robotics/ftc) team. During high school, I
led my FIRST Robotics Competition team as Chief of Software for 3 years and President for the 4th. Introducing these
students to engineering as my mentors did for me has been a learning experience, and a great way to give back to the
FIRST community. I've been with the team since its founding in late 2018, and can't wait to watch how it and its
members grow.

When I'm not working on a software project or robot, I'm usually playing Factorio or CrossCode. Factorio is a
series of open-ended engineering challenges wrapped in a game that scratches the same building and puzzle-solving itch
that programming does. CrossCode is a fast-paced 2D action/puzzle RPG with a futuristic setting; quite different from
Factorio, but it has its charm all the same.

Most of my work is on my [GitLab](https://gitlab.com/open-sourceress), and some older projects are on my
[GitHub](https://github.com/octocynth).
